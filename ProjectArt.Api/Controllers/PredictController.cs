﻿using ProjectArt.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web.Http;
using ProjectArt.Api.AzureVision;
using System.Web.Mvc;
using System.Web;
using System.Web.Http.Cors;

namespace ProjectArt.Api.Controllers
{
    public class PredictController : ApiController
    {
        // GET: api/Predict
        public async Task<IEnumerable<ArtPrediction>> Post()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            var imageBytes = await provider.Contents[0].ReadAsByteArrayAsync();
            var stream = new MemoryStream(imageBytes);

            ImagePrediction pred = new ImagePrediction();
            var result = pred.SearchImage(stream);

            SaveStreamForValidation(stream);

            if (result != null && result.Count > 0)
            {
                Artportal.ArtInfo info = new Artportal.ArtInfo();
                return info.FillPrediction(result);
            }
            throw new HttpException(404, "Prediction not found");

        }

        private void SaveStreamForValidation(MemoryStream stream)
        {
            ArtDbContext context = new ArtDbContext();
            Image imageEntity = new Image()
            {
                ID = Guid.NewGuid(),
                Data = stream.ToArray()
            };

            context.Image.Add(imageEntity);
            context.SaveChanges();
        }
    }
}

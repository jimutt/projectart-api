﻿using ProjectArt.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ProjectArt.Api.Controllers
{
    public class RegisterController : ApiController
    {
        ArtDbContext context;
        public RegisterController()
        {
            context = new ArtDbContext();
        }

        [HttpGet]
        [EnableCors("*","*","*")]
        [Route("api/registersight/{email}/{taxId}/{name}")]
        public bool RegisterSight(string email, string taxId, string name)
        {
            var currentUser = context.Users.Where(x => x.Email == email).FirstOrDefault();
            if (currentUser == null)
            {
                currentUser = context.Users.Add(new Models.User() { Email = Guid.NewGuid().ToString() });
            }
            RegisterFind(currentUser, new Spice() { Name = name, Taxkey = taxId });

            return true;
        }

        private void RegisterFind(User currentUser, Spice spiece)
        {
            context.Spice.Add(spiece);

            var challenges = GetUserChallanges(currentUser, spiece.Taxkey);

            if (challenges.Count() > 0)
            {
                foreach (var item in challenges)
                {
                    var existsing = item.ChallangeProgress.Where(z => z.User.Id == currentUser.Id).FirstOrDefault();
                    if (existsing == null)
                    {
                        item.ChallangeProgress.Add(new ChallangeProgress()
                        {
                            Found = ConvertSpiece(spiece),
                            User = currentUser
                        });
                    }
                    else
                    {
                        existsing.Found.AddRange(ConvertSpiece(spiece));
                        if (existsing.Found.Count() == 10)
                        {
                            item.Completed = true;
                        }
                    }
                    
                }
            }
            context.SaveChanges();
        }

        private List<AzureSpiceBank> ConvertSpiece(Spice spiece)
        {
            return context.ArtBank.Where(x => x.Taxkey == spiece.Taxkey).ToList();
        }

        private List<CountyChallenge> GetUserChallanges(User currentUser, string taxKey)
        {
            var onGoingChallange = context.CountyChallenges.Where(x => x.ChallangeParticipants.Any(v => v.Id == currentUser.Id) && x.Completed == false).ToList();

            return onGoingChallange.Where(x => x.SpeciesGoals.Any(v => v.Taxkey == taxKey)).ToList();

        }
    }
}

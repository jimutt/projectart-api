﻿using ProjectArt.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectArt.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            // CountyCompetitionController n = new CountyCompetitionController();
            //var fff = n.Get();
            //var testImage = new MemoryStream(File.ReadAllBytes(@"D:\UploadedToAzure\Amerikansk kricka,205518\1754814.jpg"));
            //ProjectArt.Api.AzureVision.ImagePrediction pred = new ProjectArt.Api.AzureVision.ImagePrediction();
            //var returs = pred.SearchImage(testImage);
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}

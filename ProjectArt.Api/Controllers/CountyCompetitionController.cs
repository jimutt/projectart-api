﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ProjectArt.Api.Models;

namespace ProjectArt.Api.Controllers
{
    public class CountyCompetitionController : ApiController
    {
        ArtDbContext context;
        public CountyCompetitionController()
        {
            context = new ArtDbContext();
            EndChallanges();
        }

        private void EndChallanges()
        {
            var endChallagnes = context.CountyChallenges.Where(x => x.EndDate < DateTime.Now).ToList();
            foreach (var item in endChallagnes)
            {
                item.Completed = true;
                item.ChallangeParticipants.Clear();
            }
            context.SaveChanges();
            //context.CountyChallenges.Where(x >= x.StartDate && s.Date <= currentChallange.EndDate)
        }
        public CountyChallenge CreateChallange(string Title, string county, DateTime start, DateTime end, int reward)
        {
            var game = new CountyChallenge()
            {
                Id = Guid.NewGuid(),
                Title = Title,
                County = county,
                StartDate = start,
                EndDate = end,
                RewardPoints = reward,
                SpeciesGoals = GetGoals()
            };
            context.CountyChallenges.Add(game);
            context.SaveChanges();
            return game;
        }

        public IEnumerable<CountyChallenge> Get()
        {
            return context.CountyChallenges.ToList();
        }

        [Route("countycompetition/GetCompletedChallanges")]
        public IEnumerable<CountyChallenge> GetCompletedChallanges()
        {
            var challange = context.CountyChallenges.Where(x => x.Completed == true);

            return challange;

        }

        [Route("countycompetition/ChallangeProgress")]
        public List<ChallangeProgress> ChallangeProgress(Guid CountyChallangeID)
        {
            List<ChallangeProgress> prog = new List<Models.ChallangeProgress>();
            var currentChallange = context.CountyChallenges.Where(x => x.Id == CountyChallangeID).FirstOrDefault();
            return currentChallange.ChallangeProgress.ToList();
        }

        [HttpGet]
        [Route("api/countycompetition/apply/{UserID}/{CountyChallangeID}")]
        [EnableCors("*", "*", "*")]
        public bool ApplyForChallange(string UserID, Guid CountyChallangeID)
        {
            var cha = context.CountyChallenges.Where(x => x.Id == CountyChallangeID).FirstOrDefault();
            var user = GetUser(UserID);

            if (cha.EndDate >= DateTime.Now)
            {
                if (cha.ChallangeParticipants.Any(x => x == user))
                {
                    return true;
                }
                cha.ChallangeParticipants.Add(user);
                context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }

        }

        private User GetUser(string UserID)
        {
            var user = context.Users.Where(x => x.Email == UserID).FirstOrDefault();
            if (user == null)
            {
                context.Users.Add(new Models.User()
                {
                    Email = UserID,
                    Id = Guid.NewGuid()
                });
                context.SaveChanges();
                user = context.Users.Where(x => x.Email == UserID).FirstOrDefault();
            }
            return user;
        }

        private ICollection<AzureSpiceBank> GetGoals()
        {
            var speice = context.ArtBank.ToList();
            List<AzureSpiceBank> res = new List<AzureSpiceBank>();
            var result = Enumerable.Range(1, speice.Count()).OrderBy(g => Guid.NewGuid()).Take(10).ToArray();
            foreach (var index in result)
            {
                var sp = speice[index];
                res.Add(sp);
            }
            return res;
        }

    }
}

﻿using Microsoft.Cognitive.CustomVision.Prediction;
using Microsoft.Cognitive.CustomVision.Prediction.Models;
using Microsoft.Cognitive.CustomVision.Training;
using Microsoft.Cognitive.CustomVision.Training.Models;
using ProjectArt.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.AzureVision
{
    public class ImagePrediction
    {
        public List<ImageTagPredictionModel> SearchImage(MemoryStream testimage)
        {
            List<ImageTagPredictionModel> results = new List<ImageTagPredictionModel>();
            var defs = MockDef();

            foreach (var item in defs)
            {
                MemoryStream keepStremOpen = new MemoryStream();
                testimage.CopyTo(keepStremOpen);
                keepStremOpen.Position = 0;
                results.AddRange(MakePrediction(item.ProjectID, item.PredictionKey, item.TraningKey, keepStremOpen));
                testimage.Position = 0;
            }

            return results.OrderByDescending(x => x.Probability).Take(10).ToList();

        }
        public void FillLocalCompetitionBank()
        {
            var moc = MockDef();
            ArtDbContext cont = new ArtDbContext();
            var bank = cont.ArtBank;
            foreach (var item in moc)
            {
                TrainingApi trainingApi = new TrainingApi() { ApiKey = item.TraningKey };
                var projects = trainingApi.GetProjects();
                foreach (var project in projects)
                {
                    var tags = trainingApi.GetTags(project.Id);
                    foreach (var tag in tags.Tags)
                    {
                        if (!cont.ArtBank.Any(x => x.Taxkey == tag.Name))
                        {
                            Artportal.ArtInfo info = new Artportal.ArtInfo();
                            var dyn = info.FindSpice(tag.Name);
                            string sweName = "";
                            if (dyn != null && dyn[0].swedishName != null)
                            {
                                sweName = dyn[0].swedishName;
                            }

                            cont.ArtBank.Add(new AzureSpiceBank()
                            {
                                ID = Guid.NewGuid(),
                                Name = sweName,
                                Taxkey = tag.Name
                            });
                        }
                    }
                }
                cont.SaveChanges();
            }
        }

        public List<ImageTagPredictionModel> MakePrediction(Guid projectId, string predictionKey, string traningKey, MemoryStream testImage)
        {
            //might be slow, Just for validation that project is trained, else there will be error in predictImage
            Iteration iteratiuon = GetLastTrainedIteration(traningKey, projectId);

            PredictionEndpoint endpoint = new PredictionEndpoint() { ApiKey = predictionKey };
            var result = endpoint.PredictImage(projectId, testImage, iteratiuon.Id);

            return result.Predictions.ToList();
        }
        private Iteration GetLastTrainedIteration(string traningKey, Guid projecktId)
        {
            TrainingApi trainingApi = new TrainingApi() { ApiKey = traningKey };
            var lastCompleteIteration = trainingApi.GetIterations(projecktId).Where(x => x.Status == "Completed").OrderByDescending(x => x.TrainedAt).FirstOrDefault();
            return lastCompleteIteration;
        }

        private List<AzureVisionSettings> MockDef()
        {
            List<AzureVisionSettings> settings = new List<AzureVisionSettings>();

            return settings;
        }
    }
}
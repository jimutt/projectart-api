﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Cognitive.CustomVision.Prediction.Models;
using ProjectArt.Api.Models;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Net;
using Newtonsoft.Json;
using System.Text;

namespace ProjectArt.Api.Artportal
{
    public class ArtInfo
    {
        const string PrimarySubscription = "d6cdd8a2c397471aa29613e943f737c4";
        const string SpeciesData = "https://api.artdatabanken.se/speciesdataservice/v1/speciesdata/assessments?taxa=";

        public dynamic FindSpice(string tag)
        {
            try
            {
                using (WebClient web = new WebClient())
                {
                    web.Encoding = Encoding.UTF8;
                    web.Headers.Add("Ocp-Apim-Subscription-Key", PrimarySubscription);
                    var json = web.DownloadString(SpeciesData + tag);
                    dynamic dynObj = JsonConvert.DeserializeObject(json);
                    return dynObj;
                }
            }
            catch (Exception)
            {
                //There is an error whit a tag containing invalid data ,
                return null;
            }

        }

        public IEnumerable<ArtPrediction> FillPrediction(List<ImageTagPredictionModel> result)
        {
            List<ArtPrediction> predictions = new List<ArtPrediction>();
            foreach (var item in result)
            {
                dynamic res = FindSpice(item.Tag);
                ArtPrediction asr = new ArtPrediction();
                asr.Tag = item.Tag;
                asr.Probability = item.Probability;
                asr.TagId = item.TagId;
                if (res != null)
                {
                    asr.SweName = res[0].swedishName;
                    asr.LatName = res[0].scientificName;
                }
                predictions.Add(asr);
            }
            return predictions;
        }
    }
}
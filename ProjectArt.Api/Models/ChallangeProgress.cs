﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class ChallangeProgress
    {
        [Key]
        public int  ID{ get; set; }
        public User User { get; set; }
        public List<AzureSpiceBank> Found{ get; set; }
    }
}
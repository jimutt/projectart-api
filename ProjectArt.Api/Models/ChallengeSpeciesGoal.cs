﻿ using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class ChallengeSpeciesGoal
    {
        [Key]
        public Guid Id { get; set; }
        public int TaxonId { get; set; }
        public string TaxonTitle { get; set; }
    }
}
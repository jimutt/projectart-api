﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class CreditReward
    {
        public Guid Id { get; set; }
        public int Amount { get; set; }
        public DateTime Timestamp { get; set; }
        public virtual User User { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class CountyChallenge
    {
        [Key]
        public Guid Id { get; set; }
        public string County { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public int RewardPoints { get; set; }
        public bool Completed { get; set; }

        public virtual ICollection<AzureSpiceBank> SpeciesGoals { get; set; }

        public virtual ICollection<User> ChallangeParticipants { get; set; }
        public virtual ICollection<ChallangeProgress> ChallangeProgress { get; set; }
}
}
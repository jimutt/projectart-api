﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Permissions;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class ArtDbContext : DbContext
    {
        public ArtDbContext() : base("name=ArtDb")
        {

        }

        public DbSet<CountyChallenge> CountyChallenges { get; set; }
        public DbSet<CreditReward> CreditRewards { get; set; }
        public DbSet<User> Users { get; set; }
        //public DbSet<ChallengeSpeciesGoal> ChallengeSpeciesGoals { get; set; }
        public DbSet<AzureVisionSettings> AzureVisionSettings { get; set; }
        public DbSet<AzureSpiceBank> ArtBank { get; set; }
        public DbSet<Sights> Sights { get; set; }
        public DbSet<Spice> Spice { get; set; }
        public DbSet<ChallangeProgress> ChallengeProgress { get; set; }
        public DbSet<Image> Image { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class Spice
    {
        [Key]
        public int ID { get; set; }
        public string Taxkey { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
    }
}
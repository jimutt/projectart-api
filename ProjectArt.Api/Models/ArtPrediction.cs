﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class ArtPrediction
    {
        public Guid TagId { get; set; }
        public string Tag { get; set; }
        public double Probability { get; set; }

        public string SweName { get; set; }
        public string LatName { get; set; }
        public string Description { get; set; }
        public string Tax { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class AzureVisionSettings
    {
        [Key]
        public int ID{ get; set; }
        public Guid ProjectID{ get; set; }
        public string PredictionKey { get; set; }
        public string TraningKey { get; set; }
        public Guid LastIterationID { get; set; }

        public string ReggedMail { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class Image
    {
        [Key]
        public Guid ID { get; set; }
        public string Filename { get; set; }
        public byte[] Data { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class Sights
    {
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public Spice Spiece { get; set; }
        public User user { get; set; }
    }
}
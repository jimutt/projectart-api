﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectArt.Api.Models
{
    public class AzureSpiceBank
    {
        [Key]
        public Guid ID { get; set; }
        public string Taxkey { get; set; }

        public string Image { get; set; }
        public string Name { get; set; }
    }
}
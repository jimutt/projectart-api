namespace ProjectArt.Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AzureVisionSettings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProjectID = c.Guid(nullable: false),
                        PredictionKey = c.String(),
                        TraningKey = c.String(),
                        LastIterationID = c.Guid(nullable: false),
                        ReggedMail = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ChallengeSpeciesGoals",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TaxonId = c.Int(nullable: false),
                        TaxonTitle = c.String(),
                        CountyChallenge_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CountyChallenges", t => t.CountyChallenge_Id)
                .Index(t => t.CountyChallenge_Id);
            
            CreateTable(
                "dbo.CountyChallenges",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        County = c.String(),
                        Description = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Title = c.String(),
                        Icon = c.String(),
                        RewardPoints = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CreditRewards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Amount = c.Int(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CreditRewards", "User_Id", "dbo.Users");
            DropForeignKey("dbo.ChallengeSpeciesGoals", "CountyChallenge_Id", "dbo.CountyChallenges");
            DropIndex("dbo.CreditRewards", new[] { "User_Id" });
            DropIndex("dbo.ChallengeSpeciesGoals", new[] { "CountyChallenge_Id" });
            DropTable("dbo.Users");
            DropTable("dbo.CreditRewards");
            DropTable("dbo.CountyChallenges");
            DropTable("dbo.ChallengeSpeciesGoals");
            DropTable("dbo.AzureVisionSettings");
        }
    }
}

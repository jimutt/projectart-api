namespace ProjectArt.Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ChallengeSpeciesGoals", newName: "AzureSpiceBanks");
            CreateTable(
                "dbo.ChallangeProgresses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        User_Id = c.Guid(),
                        CountyChallenge_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .ForeignKey("dbo.CountyChallenges", t => t.CountyChallenge_Id)
                .Index(t => t.User_Id)
                .Index(t => t.CountyChallenge_Id);
            
            CreateTable(
                "dbo.Sights",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Spiece_ID = c.Int(),
                        user_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Spices", t => t.Spiece_ID)
                .ForeignKey("dbo.Users", t => t.user_Id)
                .Index(t => t.Spiece_ID)
                .Index(t => t.user_Id);
            
            CreateTable(
                "dbo.Spices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Taxkey = c.String(),
                        Image = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.CountyChallenges", "Completed", c => c.Boolean(nullable: false));
            AddColumn("dbo.Users", "CountyChallenge_Id", c => c.Guid());
            AddColumn("dbo.AzureSpiceBanks", "Taxkey", c => c.String());
            AddColumn("dbo.AzureSpiceBanks", "Image", c => c.String());
            AddColumn("dbo.AzureSpiceBanks", "Name", c => c.String());
            AddColumn("dbo.AzureSpiceBanks", "ChallangeProgress_ID", c => c.Int());
            CreateIndex("dbo.AzureSpiceBanks", "ChallangeProgress_ID");
            CreateIndex("dbo.Users", "CountyChallenge_Id");
            AddForeignKey("dbo.Users", "CountyChallenge_Id", "dbo.CountyChallenges", "Id");
            AddForeignKey("dbo.AzureSpiceBanks", "ChallangeProgress_ID", "dbo.ChallangeProgresses", "ID");
            DropColumn("dbo.AzureSpiceBanks", "TaxonId");
            DropColumn("dbo.AzureSpiceBanks", "TaxonTitle");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AzureSpiceBanks", "TaxonTitle", c => c.String());
            AddColumn("dbo.AzureSpiceBanks", "TaxonId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Sights", "user_Id", "dbo.Users");
            DropForeignKey("dbo.Sights", "Spiece_ID", "dbo.Spices");
            DropForeignKey("dbo.ChallangeProgresses", "CountyChallenge_Id", "dbo.CountyChallenges");
            DropForeignKey("dbo.ChallangeProgresses", "User_Id", "dbo.Users");
            DropForeignKey("dbo.AzureSpiceBanks", "ChallangeProgress_ID", "dbo.ChallangeProgresses");
            DropForeignKey("dbo.Users", "CountyChallenge_Id", "dbo.CountyChallenges");
            DropIndex("dbo.Sights", new[] { "user_Id" });
            DropIndex("dbo.Sights", new[] { "Spiece_ID" });
            DropIndex("dbo.ChallangeProgresses", new[] { "CountyChallenge_Id" });
            DropIndex("dbo.ChallangeProgresses", new[] { "User_Id" });
            DropIndex("dbo.Users", new[] { "CountyChallenge_Id" });
            DropIndex("dbo.AzureSpiceBanks", new[] { "ChallangeProgress_ID" });
            DropColumn("dbo.AzureSpiceBanks", "ChallangeProgress_ID");
            DropColumn("dbo.AzureSpiceBanks", "Name");
            DropColumn("dbo.AzureSpiceBanks", "Image");
            DropColumn("dbo.AzureSpiceBanks", "Taxkey");
            DropColumn("dbo.Users", "CountyChallenge_Id");
            DropColumn("dbo.CountyChallenges", "Completed");
            DropTable("dbo.Spices");
            DropTable("dbo.Sights");
            DropTable("dbo.ChallangeProgresses");
            RenameTable(name: "dbo.AzureSpiceBanks", newName: "ChallengeSpeciesGoals");
        }
    }
}

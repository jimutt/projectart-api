namespace ProjectArt.Api.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class imgnew : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        Filename = c.String(),
                        Data = c.Binary(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Images");
        }
    }
}
